/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.senac.test;

import br.com.senac.SituacaoAluno;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author sala304b
 */
public class SituacaoAlunoTest {
    
    public SituacaoAlunoTest() {
    }
    
    @Test
    public void deveSerAprovadoAlunoComNota7Acima(){
        int nota = 7;
        String resultado = SituacaoAluno.verificarSituacao(nota);
        assertEquals(resultado, SituacaoAluno.ALUNO_APROVADO);
    }
    
    @Test
    public void deveSerReprovadoAlunoComNotaMenorQue4(){
        int nota = 3;
        String resultado = SituacaoAluno.verificarSituacao(nota);
        assertEquals(resultado, SituacaoAluno.ALUNO_REPROVADO);
    }
    
    @Test
    public void deveEstarDeRecuperacaoAlunoComNotaEntre4E6(){
        int nota = 5;
        String resultado = SituacaoAluno.verificarSituacao(nota);
        assertEquals(resultado, SituacaoAluno.ALUNO_RECUPERACAO);
    }
}
