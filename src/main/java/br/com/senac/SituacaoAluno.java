/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.senac;

/**
 *
 * @author sala304b
 */
public class SituacaoAluno {

    public static final String ALUNO_APROVADO = "Aprovado";
    public static final String ALUNO_RECUPERACAO = "Recuperação";
    public static final String ALUNO_REPROVADO = "Reprovado";

    public static String verificarSituacao(int nota) {
        if (nota >= 7) {
            return SituacaoAluno.ALUNO_APROVADO;
        } else if (nota < 4) {
            return SituacaoAluno.ALUNO_REPROVADO;
        } else {
            return SituacaoAluno.ALUNO_RECUPERACAO;
        }
    }

}
